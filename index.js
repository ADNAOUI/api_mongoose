require('dotenv').config();
const mongoString = process.env.DATABASE_URL;

const express = require('express');
const mongoose = require('mongoose');
const routes = require('./routes/routes');
const Handlebars = require('handlebars')
const expressHbs = require('express-handlebars');
const {allowInsecurePrototypeAccess} = require('@handlebars/allow-prototype-access')

mongoose.connect(mongoString);
const database = mongoose.connection;

database.on('error', (error) => {
    console.log(error)
})

database.once('connected', () => {
    console.log('Database Connected');
})

const app = express();

app.use(express.json());
app.use('/api', routes);
app.use(function(req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});
app.use(function(err, req, res) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: err
    });
});
app.listen(3000, () => {
    console.log(`Server Started at ${3000}`)
})
app.engine('.hbs', expressHbs.engine({defaultLayout: 'layout', extname: '.hbs',  handlebars: allowInsecurePrototypeAccess(Handlebars)}));
app.set('view engine', '.hbs')