const mongoose = require('mongoose');

const animalSchema = new mongoose.Schema({
        name: {
            required: true,
            type: String
        },
        species: {
            required: true,
            type: String
        },
        description: {
            type: String
        },
        image: {
            required: true,
            type: String
        }
    },
    { timestamps: true }
)

animalSchema.method("toJSON", function(){
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;

    return object;
})

module.exports = mongoose.model('Animal', animalSchema)