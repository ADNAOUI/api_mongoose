const express = require('express');
const controller = require('../controllers/');
const router = express.Router()

module.exports = router;

router.get('/', function(req, res) {
    res.render('index', { title: 'API pour les animaux' });
});

const Animal = controller.animalController
// Create and Save a new Animal
router.post('/animal', Animal.create)

// Retrieve all Animals from the database.
router.get("/animal", Animal.findAll);

// Retrieve all published Animal
router.get("/published", Animal.findAllPublished);

// Retrieve a single Animal with id
router.get("/animal/:id", Animal.findOne);

// Update a Tutorial with id
router.put("/animal/:id", Animal.update);

// Delete an Animal with id
router.delete("/animal/:id", Animal.delete);

// Create a new Animal
router.delete("/animal", Animal.deleteAll);