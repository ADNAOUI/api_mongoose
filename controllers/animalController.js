const db = require('../models/')
const Animal = db.animal
// https://www.bezkoder.com/node-express-mongodb-crud-rest-api/
// https://www.bezkoder.com/react-node-express-mongodb-mern-stack/

exports.create = (req, res) => {
    if (!req.body.name) {
        res.status(400).send({ message: "Content can not be empty!" });
        return;
    }

    const createAnimal = new Animal({
        name: req.body.name,
        species: req.body.species,
        description: req.body.description,
        image: req.body.image
    })

   createAnimal
       .save(createAnimal)
       .then(data => {
           res.send(data)
       })
       .catch(err => {
           res.status(500).send({
               message:
                   err.message || "Some error occurred while creating the Animal."
           });
       });
};

// Display with Handlebars all Animals from the database.
exports.findAll = (req, res) => {
    const title = req.query.name;
    Animal.find(title)
        .then(data => {
            res.render('animals/index', { title: 'Animals', contents: data });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving Animal."
            });
        });
};

// Find a single Animal with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
    Animal.findById(id)
        .then(data => {
            if (!data)
                res.status(404).send({ message: "Not found Animal with id " + id });
            else res.send(data);
        })
        .catch(err => {
            res
                .status(500)
                .send({ message: "Error retrieving Animal with id=" + id + err});
        });
};

// Update an Animal by the id in the request
exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!"
        });
    }
    const id = req.params.id;
    Animal.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update Animal with id=${id}. Maybe Animal was not found!`
                });
            } else res.send({ message: "Animal was updated successfully." });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Animal with id=" + id + err
            });
        });
};

// Delete an Animal with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
    Animal.findByIdAndRemove(id)
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot delete Animal with id=${id}. Maybe Animal was not found!`
                });
            } else {
                res.send({
                    message: "Animal was deleted successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Animal with id=" + id + err
            });
        });
};

// Delete all Animal from the database.
exports.deleteAll = (req, res) => {
    Animal.deleteMany({})
        .then(data => {
            res.send({
                message: `${data.deletedCount} Animals were deleted successfully!`
            });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all Animal."
            });
        });
};

// Find all published Animal
exports.findAllPublished = (req, res) => {
    Animal.find({ published: true })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving Animal."
            });
        });
};